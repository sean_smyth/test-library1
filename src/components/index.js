// components/index.js
import "./base_styles.css";
import "./component_styles.css";

import Button from "./Button";
import ButtonSmall from "./ButtonSmall";

export { Button, ButtonSmall };
