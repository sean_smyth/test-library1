// src/components/Button.js
import React from "react";

// //
// import { button_ss } from "./buttonSmall.module.scss";
//

// const ButtonEl = styled.button`
// 	display: inline-block;
// 	padding: 0.5rem;
// 	border-radius: 22rem;
// 	color: white;
// 	background-color: var(--blue);
// 	overflow: hidden;

// 	span {
// 		border: 50px solid beige;
// 		margin-right: 10px;
// 	}
// `;

export default function ButtonSmall({ text, onClick }) {
	return (
		<button className={'button-small'} onClick={onClick}>
			<span>{text}</span> | I'm a wee button, hey
		</button>
	);
}
