// src/components/Button.js
import React from "react";
// import styled from "styled-components";

//
// import { button } from "./button.module.scss";
//

// const ButtonEl = styled.button`
// 	background-color: var(--green) !important;
// 	display: inline-block;
// 	padding: 1.2rem;
// 	color: var(--white);
// 	border-radius: 22rem;

// 	span {
// 		border: 5px solid var(--blue);
// 	}
// `;

export default function Button({ text, onClick }) {
	return (
		<button onClick={onClick} className={'button'}>
			<span>{text}</span> | I'm a big button
		</button>
	);
}
