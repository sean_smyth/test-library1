import React from "react";
import logo from "./logo.svg";
import "./App.css";

import { ButtonSmall, Button } from "./components";

function App() {
	return (
		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo" alt="logo" />
				<p>This is a library</p>
				<ButtonSmall text="I'm a wee Button" />
				<Button text="I'm a normal Button" />
				<p>This is a library</p>
				<ButtonSmall text="I'm a wee Button" />
				<Button text="I'm a normal Button" />
			</header>
		</div>
	);
}

export default App;
