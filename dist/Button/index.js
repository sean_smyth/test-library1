"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Button;

var _react = _interopRequireDefault(require("react"));

var _buttonModule = require("./button.module.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// src/components/Button.js
// import styled from "styled-components";
//
//
// const ButtonEl = styled.button`
// 	background-color: var(--green) !important;
// 	display: inline-block;
// 	padding: 1.2rem;
// 	color: var(--white);
// 	border-radius: 22rem;
// 	span {
// 		border: 5px solid var(--blue);
// 	}
// `;
function Button(_ref) {
  var text = _ref.text,
      onClick = _ref.onClick;
  return /*#__PURE__*/_react.default.createElement("button", {
    onClick: onClick,
    className: _buttonModule.button
  }, /*#__PURE__*/_react.default.createElement("span", null, text), " | I'm a big button");
}