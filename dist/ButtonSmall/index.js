"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ButtonSmall;

var _react = _interopRequireDefault(require("react"));

var _buttonSmallModule = require("./buttonSmall.module.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// src/components/Button.js
// import styled from "styled-components";
// //
//
// const ButtonEl = styled.button`
// 	display: inline-block;
// 	padding: 0.5rem;
// 	border-radius: 22rem;
// 	color: white;
// 	background-color: var(--blue);
// 	overflow: hidden;
// 	span {
// 		border: 50px solid beige;
// 		margin-right: 10px;
// 	}
// `;
function ButtonSmall(_ref) {
  var text = _ref.text,
      onClick = _ref.onClick;
  return /*#__PURE__*/_react.default.createElement("button", {
    className: _buttonSmallModule.button_ss,
    onClick: onClick
  }, /*#__PURE__*/_react.default.createElement("span", null, text), " | I'm a wee button, hey");
}